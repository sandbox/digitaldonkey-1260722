<?php
/**
 * @file
 * openlayers_sample.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function openlayers_sample_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_views_api().
 */
function openlayers_sample_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implementation of hook_node_info().
 */
function openlayers_sample_node_info() {
  $items = array(
    'map_sample' => array(
      'name' => t('Map Sample'),
      'base' => 'node_content',
      'description' => t('This is an Example content type for the Openlayers Module'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
