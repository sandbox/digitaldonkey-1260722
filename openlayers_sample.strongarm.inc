<?php
/**
 * @file
 * openlayers_sample.strongarm.inc
 */

/**
 * Implementation of hook_strongarm().
 */
function openlayers_sample_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_map_sample';
  $strongarm->value = 0;
  $export['comment_anonymous_map_sample'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_map_sample';
  $strongarm->value = 1;
  $export['comment_default_mode_map_sample'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_map_sample';
  $strongarm->value = '50';
  $export['comment_default_per_page_map_sample'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_map_sample';
  $strongarm->value = 1;
  $export['comment_form_location_map_sample'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_map_sample';
  $strongarm->value = '1';
  $export['comment_map_sample'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_map_sample';
  $strongarm->value = '1';
  $export['comment_preview_map_sample'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_map_sample';
  $strongarm->value = 1;
  $export['comment_subject_field_map_sample'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_map_sample';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_map_sample'] = $strongarm;

  return $export;
}
