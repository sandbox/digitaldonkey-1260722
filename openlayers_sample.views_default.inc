<?php
/**
 * @file
 * openlayers_sample.views_default.inc
 */

/**
 * Implementation of hook_views_default_views().
 */
function openlayers_sample_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'map_sample_data';
  $view->description = 'Example Open Layers Date View';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Map Sample Data';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Map Sample';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'openlayers_map';
  $handler->display->display_options['style_options']['map'] = 'sample_map';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Latitude */
  $handler->display->display_options['fields']['field_lat']['id'] = 'field_lat';
  $handler->display->display_options['fields']['field_lat']['table'] = 'field_data_field_lat';
  $handler->display->display_options['fields']['field_lat']['field'] = 'field_lat';
  $handler->display->display_options['fields']['field_lat']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_lat']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_lat']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_lat']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_lat']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_lat']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_lat']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_lat']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_lat']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  $handler->display->display_options['fields']['field_lat']['field_api_classes'] = 0;
  /* Field: Content: Longitude */
  $handler->display->display_options['fields']['field_long']['id'] = 'field_long';
  $handler->display->display_options['fields']['field_long']['table'] = 'field_data_field_long';
  $handler->display->display_options['fields']['field_long']['field'] = 'field_long';
  $handler->display->display_options['fields']['field_long']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_long']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_long']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_long']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_long']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_long']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_long']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_long']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_long']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  $handler->display->display_options['fields']['field_long']['field_api_classes'] = 0;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'map_sample' => 'map_sample',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = FALSE;
  $handler->display->display_options['header']['area']['content'] = 'This Feature works on openlayers-7.x-2.x-dev only at the moment.
  You can use the Devel-Generate Module to create some sample data  in the "Map Sample" content type.';
  $handler->display->display_options['header']['area']['format'] = 'plain_text';
  $handler->display->display_options['header']['area']['tokenize'] = 0;
  $handler->display->display_options['path'] = 'map-sample-data';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Map Sample Data';
  $handler->display->display_options['menu']['name'] = 'main-menu';

  /* Display: OpenLayers Data Overlay */
  $handler = $view->new_display('openlayers', 'OpenLayers Data Overlay', 'openlayers_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'openlayers_data';
  $handler->display->display_options['style_options']['data_source'] = array(
    'value' => 'other_latlon',
    'other_lat' => 'field_lat',
    'other_lon' => 'field_long',
    'wkt' => 'title',
    'other_top' => 'title',
    'other_right' => 'title',
    'other_bottom' => 'title',
    'other_left' => 'title',
    'name_field' => 'title',
    'description_field' => 'field_map_sample_image',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['title']['alter']['text'] = '[title]
';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Latitude */
  $handler->display->display_options['fields']['field_lat']['id'] = 'field_lat';
  $handler->display->display_options['fields']['field_lat']['table'] = 'field_data_field_lat';
  $handler->display->display_options['fields']['field_lat']['field'] = 'field_lat';
  $handler->display->display_options['fields']['field_lat']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_lat']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_lat']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_lat']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_lat']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_lat']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_lat']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_lat']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_lat']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_lat']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  $handler->display->display_options['fields']['field_lat']['field_api_classes'] = 0;
  /* Field: Content: Longitude */
  $handler->display->display_options['fields']['field_long']['id'] = 'field_long';
  $handler->display->display_options['fields']['field_long']['table'] = 'field_data_field_long';
  $handler->display->display_options['fields']['field_long']['field'] = 'field_long';
  $handler->display->display_options['fields']['field_long']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_long']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_long']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_long']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_long']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_long']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_long']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_long']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_long']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_long']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  $handler->display->display_options['fields']['field_long']['field_api_classes'] = 0;
  $export['map_sample_data'] = $view;

  return $export;
}
