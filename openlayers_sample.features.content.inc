<?php
/**
 * @file
 * openlayers_sample.features.content.inc
 */

/**
 * Implementation of hook_content_defaults().
 */
function openlayers_sample_content_defaults() {
$content = array();
$content['amsterdam2005'] = (object)array(
  'title' => 'Amsterdam 2005',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314361314',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'amsterdam2005',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '100 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>100 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '52.3738',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '4.89093',
      ),
    ),
  ),
);
$content['antwerp2005'] = (object)array(
  'title' => 'Antwerp 2005',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314361475',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'antwerp2005',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '50 attendees.  ',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>50 attendees.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '51.2206',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '4.39972',
      ),
    ),
  ),
);
$content['barcelona2007'] = (object)array(
  'title' => 'Barcelona 2007',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314360758',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'barcelona2007',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '450 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>450 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '41.3879',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '2.16992',
      ),
    ),
  ),
);
$content['boston2008'] = (object)array(
  'title' => 'Boston 2008',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314360694',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'boston2008',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '850 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>850 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '42.3584',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '-71.0598',
      ),
    ),
  ),
);
$content['brussels2006'] = (object)array(
  'title' => 'Brussels 2006',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314361202',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'brussels2006',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '150 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>150 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '50.8463',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '4.35473',
      ),
    ),
  ),
);
$content['chicago2011'] = (object)array(
  'title' => 'Chicago 2011',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314360059',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'chicago2011',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '3000 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>3000 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '41.8781',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '-87.6298',
      ),
    ),
  ),
);
$content['copenhagen2010'] = (object)array(
  'title' => 'Copenhagen 2010',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314360185',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'copenhagen2010',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '1200 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>1200 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '55.6763',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '12.5681',
      ),
    ),
  ),
);
$content['denver2012'] = (object)array(
  'title' => 'Denver 2012',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314359881',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'denver2012',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'Looking forward to it already!',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>Looking forward to it already!</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '39.7392',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '-104.985',
      ),
    ),
  ),
);
$content['london2011'] = (object)array(
  'title' => 'London 2011',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314359973',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'london2011',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '1751 Attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>1751 Attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '51.5002',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '-0.126236',
      ),
    ),
  ),
);
$content['munich2012'] = (object)array(
  'title' => 'Munich 2012',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314361602',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'munich2012',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'looking forward to it already!!',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>looking forward to it already!!</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '48.1391',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '11.5802',
      ),
    ),
  ),
);
$content['paris2009'] = (object)array(
  'title' => 'Paris 2009',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314360372',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'paris2009',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '850 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>850 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '48.8567',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '2.35099',
      ),
    ),
  ),
);
$content['portland2005'] = (object)array(
  'title' => 'Portland 2005',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314361404',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'portland2005',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '100 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>100 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '45.5235',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '-122.676',
      ),
    ),
  ),
);
$content['sanfrancisco2006'] = (object)array(
  'title' => 'San Francisco 2010',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314360257',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'sanfrancisco2006',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '3000 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>3000 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '37.7749',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '-122.419',
      ),
    ),
  ),
);
$content['sunnyvale2007'] = (object)array(
  'title' => 'Sunnyvale 2007',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314361037',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'sunnyvale2007',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '300 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>300 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '37.3688',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '-122.036',
      ),
    ),
  ),
);
$content['szeged2008'] = (object)array(
  'title' => 'Szeged 2008',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314360603',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'szeged2008',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '500 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>500 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '46.2537',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '20.146',
      ),
    ),
  ),
);
$content['vancouver2006'] = (object)array(
  'title' => 'Vancouver 2006',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314361261',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'vancouver2006',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '150 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>150 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '49.2636',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '-123.139',
      ),
    ),
  ),
);
$content['washingtondc2009'] = (object)array(
  'title' => 'Washington, D.C. 2009',
  'status' => '1',
  'promote' => '1',
  'sticky' => '0',
  'type' => 'map_sample',
  'language' => 'und',
  'created' => '1314360525',
  'comment' => '1',
  'translate' => '0',
  'machine_name' => 'washingtondc2009',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '1400 attendees',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>1400 attendees</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_lat' => array(
    'und' => array(
      0 => array(
        'value' => '38.8951',
      ),
    ),
  ),
  'field_long' => array(
    'und' => array(
      0 => array(
        'value' => '-77.0364',
      ),
    ),
  ),
);
return $content;
}
